use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Deserialize, Serialize)]
struct Input {
    digit_frequencies: BTreeMap<char, usize>,
}

#[derive(Deserialize, Serialize)]
struct Output {
    low_frequency_digits: BTreeMap<char, usize>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(filter_low_frequency_digits);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn filter_low_frequency_digits(event: Input, _ctx: Context) -> Result<Output, Error> {
    let mut low_frequency_digits = BTreeMap::new();
    for (char, count) in event.digit_frequencies {
        if count <= 2 {
            low_frequency_digits.insert(char, count);
        }
    }

    Ok(Output { low_frequency_digits })
}
