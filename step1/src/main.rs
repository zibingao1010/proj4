use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, BTreeMap};

#[derive(Deserialize, Serialize)]
struct Input {
    text: String,
}

#[derive(Deserialize, Serialize)]
struct Output {
    digit_frequencies: BTreeMap<char, usize>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(count_digits);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn count_digits(event: Input, _ctx: Context) -> Result<Output, Error> {
    let mut counts = HashMap::new();
    for char in event.text.chars() {
        if char.is_digit(10) { // Check if the character is a digit
            *counts.entry(char).or_insert(0) += 1;
        }
    }
    let sorted_counts: BTreeMap<_, _> = counts.into_iter().collect();

    Ok(Output { digit_frequencies: sorted_counts })
}
