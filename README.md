# IDS721 Individual Project 4

## Rust AWS Lambda function
1. Use `cargo lambda new <PROJECT_NAME>` to create lambda project.
2. Add necessary dependencies to `Cargo.toml`.
3. Implement functions in `main.rs`.
4. Run `cargo lambda watch` to test lambda functions locally. 

![image](image1.png)

5. Create an IAM user with policies `AWSLambda_FullAccess`, `AWSLambdaBasicExecutionRole`, `IAMFullAccess`.
6. Go to `Secruity Credentials` to create new access key.
7. Run `aws conigure` to enter the newly created access key.
8. Obtain the binary file by building the project: `cargo lambda build --release`
9. Deploy to lambda: `cargo lambda deploy --region <REGION>`
10. Do the same thing for both lambda functions.

![image](image2.png)

### Step Functions workflow coordinating Lambdas
1. Create a new State Machine in AWS Step Functions using the two Lambda functions.
2. Test with input and see if the test has passed.

![image](image3.png)

![image](image4.png)

## Orchestrate data processing pipeline
My data processing pipeline is like this:
```json
{
    "Comment": "In the bustling city of Riverdale, the population soared to 57,324 people last year, an increase of 8.3% from the previous year. The local library reported a record high of 12,489 book loans in March alone, while the city park welcomed over 150,000 visitors during the summer festival. At the corner of 5th Avenue and Maple Street, the new coffee shop, Brew & Bytes, sold exactly 1,287 cups of their signature caramel macchiato last Monday. Meanwhile, Riverdale High School celebrated its 50th anniversary with a grand reunion that saw 750 alumni return to their alma mater. The event raised $24,500 for school renovations, ensuring that future generations will also enjoy the historic campus.",
    "StartAt": "step1",
    "States": {
        "step1": {
            "Type": "Task",
            "Resource": "<FIRST_LAMBDA_ARN>",
            "Next": "step2"
        },
        "step2": {
            "Type": "Task",
            "Resource": "<SECOND_LAMBDA_ARN>",
            "End": true
        }
    }
}
```

## Demo Video
[Link](https://gitlab.com/zibingao1010/proj4/-/blob/main/Screen%20Recording%202024-04-15%20at%2020.26.30.mov?ref_type=heads)